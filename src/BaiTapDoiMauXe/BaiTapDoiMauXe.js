import React, { Component } from 'react';
import { connect } from 'react-redux';
import actionConstant from './constants/global';

class BaiTapDoiMauXe extends Component {
    render() {
        return (
            <div className="container d-flex align-items-center py-5">
                <img src={this.props.imgCarUrl} alt="" className="w-75" />
                <div className="w-25">
                    <button onClick={this.props.handleChangeRedCar} className="btn btn-danger">Red</button>
                    <button onClick={this.props.handleChangeBlackCar} className="btn btn-dark mx-3">Black</button>
                    <button onClick={this.props.handleChangeSilverCar} className="btn btn-secondary">Silver</button>
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        imgCarUrl: state.CarImageToScreen.imgSrc,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleChangeRedCar: () => {
            dispatch({ type: actionConstant.RED_CAR });
        },

        handleChangeBlackCar: () => {
            dispatch({ type: actionConstant.BLACK_CAR });
        },

        handleChangeSilverCar: () => {
            dispatch({ type: actionConstant.SILVER_CAR });
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BaiTapDoiMauXe);