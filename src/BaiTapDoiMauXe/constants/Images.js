const images = {
    RED_CAR: "./Car_images/red-car.jpg",
    BLACK_CAR: "./Car_images/black-car.jpg",
    SILVER_CAR: "./Car_images/silver-car.jpg"
}

export default images;