const actionConstant = {
    RED_CAR: "change to red car",
    BLACK_CAR: "change to black car",
    SILVER_CAR: "change to silver car"
}

export default actionConstant;