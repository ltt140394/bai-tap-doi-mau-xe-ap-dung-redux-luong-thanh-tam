import actionConstant from "../constants/global";
import images from "../constants/Images";

let initialState = {
    imgSrc: images.RED_CAR,
}

export const CarImageToScreen = (state = initialState, action) => {
    switch (action.type) {
        case actionConstant.RED_CAR: {
            state.imgSrc = images.RED_CAR;
            return { ...state };
        }

        case actionConstant.BLACK_CAR: {
            state.imgSrc = images.BLACK_CAR;
            return { ...state };
        }

        case actionConstant.SILVER_CAR: {
            state.imgSrc = images.SILVER_CAR;
            return { ...state };
        }

        default: return state;
    }
}